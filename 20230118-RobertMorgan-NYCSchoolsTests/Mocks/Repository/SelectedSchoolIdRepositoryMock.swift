//
//  SelectedSchoolIdRepositoryMock.swift
//  20230118-RobertMorgan-NYCSchoolsTests
//
//  Created by Roberto Moran on 1/19/23.
//

@testable import _0230118_RobertMorgan_NYCSchools
import Foundation
import Combine

class SelectedSchoolIdRepositoryMock: SelectedSchoolIdRepositoryContract {
    
    var schoolId: String?
    
    func persist(_ id: String) {
        schoolId = id
    }
    
    func fetchSelectedSchoolId() -> String? {
        return schoolId
    }
}
