//
//  SchoolsRepositoryMock.swift
//  20230118-RobertMorgan-NYCSchoolsTests
//
//  Created by Roberto Moran on 1/18/23.
//

@testable import _0230118_RobertMorgan_NYCSchools
import Foundation
import Combine

enum ErrorResponseMock: Error {
    case mockError
}

class SchoolsRepositoryMock: SchoolsRepository {
    
    override func fetchSchools() -> AnyPublisher<[School], Error> {
        return Future<[School], Error> { promise in
            if self.shouldFail {
                promise(.failure(ErrorResponseMock.mockError))
            } else {
                promise(.success(self.schools))
            }
        }.eraseToAnyPublisher()
    }
    
    var shouldFail: Bool = false
    var schools: [School] = []
    
}
