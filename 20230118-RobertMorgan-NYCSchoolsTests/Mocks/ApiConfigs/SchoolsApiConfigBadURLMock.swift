//
//  SchoolsApiConfigBadURLMock.swift
//  20230118-RobertMorgan-NYCSchoolsTests
//
//  Created by Roberto Moran on 1/18/23.
//

@testable import _0230118_RobertMorgan_NYCSchools
import Foundation

struct SchoolsApiConfigBadURLMock: SchoolsApiConfigContract {
    func getSchoolsUrl() -> URL? {
        nil
    }
}
