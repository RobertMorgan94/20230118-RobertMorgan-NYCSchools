//
//  SchoolDetailsRemoteDataSourceMock.swift
//  20230118-RobertMorgan-NYCSchoolsTests
//
//  Created by Roberto Moran on 1/18/23.
//

@testable import _0230118_RobertMorgan_NYCSchools
import Foundation
import Combine

enum errorResponseMock: Error {
    case mockError
}

class SchoolDetailsRemoteDataSourceMock: SchoolDetailsRemoteDataSourceContract {
    
    func fetchDetailsFor(schoolWith id: String) -> AnyPublisher<_0230118_RobertMorgan_NYCSchools.SchoolDetails, Error> {
        return Future<SchoolDetails, Error> { promise in
            if self.shouldFail {
                promise(.failure(ErrorResponseMock.mockError))
            } else {
                promise(.success(self.schoolDetails))
            }
        }.eraseToAnyPublisher()
    }
    
    var shouldFail: Bool = false
    
    var schoolDetails: SchoolDetails = SchoolDetails(
        schoolId: nil,
        schoolName: nil,
        satTestTakers: nil,
        satReadingScore: nil,
        satMathScore: nil,
        satWritingScore: nil
    )
}
