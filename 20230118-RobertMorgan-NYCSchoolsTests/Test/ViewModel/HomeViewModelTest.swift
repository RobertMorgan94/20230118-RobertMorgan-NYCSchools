//
//  HomeViewModelTest.swift
//  20230118-RobertMorgan-NYCSchoolsTests
//
//  Created by Roberto Moran on 1/18/23.
//

@testable import _0230118_RobertMorgan_NYCSchools
import XCTest
import Combine

final class HomeViewModelTest: XCTestCase {
    
    var sut: HomeViewModel!
    var schoolsRepository: SchoolsRepositoryMock!
    var subcription: Set<AnyCancellable>!
    
    override func setUpWithError() throws {
        try super.setUpWithError()
        schoolsRepository = SchoolsRepositoryMock()
        DependencyInitializer.container.register(SchoolsRepositoryContract.self) {
            self.schoolsRepository
        }
        sut = HomeViewModel()
        self.subcription = []
    }
    
    override func tearDownWithError() throws {
        sut = nil
        schoolsRepository = nil
        subcription = nil
        try super.tearDownWithError()
    }
    
    func test_whenRepositoryFails_fetchSchoolsPublishesFailureState() {
        //Given
        let completionExpectation = XCTestExpectation(description: "call should emit failure")
        schoolsRepository.shouldFail = true
        
        //When
        sut.fetchSchools()
        
        sut.$state.sink { state in
            //Then
            if case .error = state {
                completionExpectation.fulfill()
            }
            if case .success(_) = state {
                XCTFail("state shouldnt get to success")
                completionExpectation.fulfill()
            }
        }.store(in: &subcription)
        
        
        wait(for: [completionExpectation], timeout: 1)
    }
    
    func test_whenRepositorySuccess_fetchSchoolsPublishersEmptyResponse() {
        //Given
        let completionExpectation = XCTestExpectation(description: "call should emit failure")
        schoolsRepository.shouldFail = false
        schoolsRepository.schools = []
        
        //When
        sut.fetchSchools()
        
        sut.$state.sink { state in
            //Then
            if case .error = state {
                completionExpectation.fulfill()
            }
            if case .success(_) = state {
                XCTFail("state shouldnt get to success")
                completionExpectation.fulfill()
            }
        }.store(in: &subcription)
        
        wait(for: [completionExpectation], timeout: 1)
    }
    
    func test_whenRepositoryReturnNonValidSchools_fetchSchoolsPublishesFailure() {
        //Given
        let completionExpectation = XCTestExpectation(description: "call should emit Failure")
        schoolsRepository.shouldFail = false
        schoolsRepository.schools = [
            School(schoolId: nil, schoolName: nil, neighborhood: nil, website: nil),
            School(schoolId: "nil", schoolName: nil, neighborhood: nil, website: nil),
            School(schoolId: nil, schoolName: "nil", neighborhood: nil, website: nil),
            School(schoolId: nil, schoolName: nil, neighborhood: "nil", website: nil),
            School(schoolId: nil, schoolName: nil, neighborhood: nil, website: "nil")
        ]
        
        //When
        sut.fetchSchools()
        
        sut.$state.sink { state in
            //Then
            if case .error = state {
                completionExpectation.fulfill()
            }
            if case .success(_) = state {
                XCTFail("state shouldnt get to success")
                completionExpectation.fulfill()
            }
        }.store(in: &subcription)
        wait(for: [completionExpectation], timeout: 1)
    }
    
    func test_whenRepositoryReturnValidSchools_fetchSchoolsPublishesSuccessWithValidSchools() {
        //Given
        let completionExpectation = XCTestExpectation(description: "call should emit Failure")
        schoolsRepository.shouldFail = false
        let schoolName1 = "BestSchool"
        let schoolName2 = "AnotherBestSchool"
        
        schoolsRepository.schools = [
            School(schoolId: "1", schoolName: schoolName1, neighborhood: "some1", website: ""),
            School(schoolId: nil, schoolName: nil, neighborhood: nil, website: nil),
            School(schoolId: "nil", schoolName: nil, neighborhood: nil, website: nil),
            School(schoolId: nil, schoolName: "nil", neighborhood: nil, website: nil),
            School(schoolId: nil, schoolName: nil, neighborhood: "nil", website: nil),
            School(schoolId: nil, schoolName: nil, neighborhood: nil, website: "nil"),
            School(schoolId: "2", schoolName: schoolName2, neighborhood: "some2", website: ""),
        ]
        
        //When
        sut.fetchSchools()
        
        sut.$state.sink { state in
            //Then
            if case .error = state {
                XCTFail("state shouldnt get to failure, but succeed")
                completionExpectation.fulfill()
            }
            if case .success(let schools) = state {
                XCTAssert(schools.count == 2)
                XCTAssertEqual(schools[0].name, schoolName1)
                XCTAssertEqual(schools[1].name, schoolName2)
                completionExpectation.fulfill()
            }
        }.store(in: &subcription)
        wait(for: [completionExpectation], timeout: 1)
    }
}
