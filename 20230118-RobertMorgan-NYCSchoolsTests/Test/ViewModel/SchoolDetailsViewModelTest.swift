//
//  SchoolDetailsViewModelTest.swift
//  20230118-RobertMorgan-NYCSchoolsTests
//
//  Created by Roberto Moran on 1/19/23.
//

@testable import _0230118_RobertMorgan_NYCSchools
import XCTest
import Combine

final class SchoolDetailsViewModelTest: XCTestCase {
    
    var sut: SchoolDetailsViewModel!
    var schoolDetailsRepository: SchoolDetailsRepositoryMock!
    var selectedSchoolRepository: SelectedSchoolIdRepositoryMock!
    var subcription: Set<AnyCancellable>!
    
    
    override func setUpWithError() throws {
        try super.setUpWithError()
        schoolDetailsRepository = SchoolDetailsRepositoryMock()
        selectedSchoolRepository = SelectedSchoolIdRepositoryMock()
        
        DependencyInitializer.container.register(SchoolDetailsRepositoryContract.self) {
            self.schoolDetailsRepository
        }
        DependencyInitializer.container.register(SelectedSchoolIdRepositoryContract.self) {
            self.selectedSchoolRepository
        }
        sut = SchoolDetailsViewModel()
        self.subcription = []
    }
    
    override func tearDownWithError() throws {
        sut = nil
        schoolDetailsRepository = nil
        selectedSchoolRepository = nil
        subcription = nil
        try super.tearDownWithError()
    }
    
    func test_whenRepositoryFails_fetchSchoolsPublishesFailureState() {
        //Given
        let completionExpectation = XCTestExpectation(description: "call should emit failure")
        schoolDetailsRepository.shouldFail = true
        
        //When
        sut.fetchSchoolDetails()
        
        sut.$state.sink { state in
            //Then
            if case .error = state {
                completionExpectation.fulfill()
            }
            if case .success(_) = state {
                XCTFail("state shouldnt get to success")
                completionExpectation.fulfill()
            }
        }.store(in: &subcription)
        
        wait(for: [completionExpectation], timeout: 1)
    }
    
    func test_whenSelectedIdIsNil_fetchSchoolDetailsPublishesFailureState() {
        //Given
        let completionExpectation = XCTestExpectation(description: "call should emit failure")
        selectedSchoolRepository.schoolId = nil
        
        //When
        sut.fetchSchoolDetails()
        
        sut.$state.sink { state in
            //Then
            if case .error = state {
                completionExpectation.fulfill()
            }
            if case .success(_) = state {
                XCTFail("state shouldnt get to success")
                completionExpectation.fulfill()
            }
        }.store(in: &subcription)
        
        wait(for: [completionExpectation], timeout: 1)
    }
    
    func test_whenSelectedSchoolNameIsNil_fetchSchoolDetailsPublishesFailureState() {
        //Given
        let completionExpectation = XCTestExpectation(description: "call should emit failure")
        schoolDetailsRepository.shouldFail = false
        schoolDetailsRepository.schoolDetails = SchoolDetails(
            schoolId: "Some",
            schoolName: nil,
            satTestTakers: "Some",
            satReadingScore: "Some",
            satMathScore: "Some",
            satWritingScore: "Some"
        )
        
        //When
        sut.fetchSchoolDetails()
        
        sut.$state.sink { state in
            //Then
            if case .error = state {
                completionExpectation.fulfill()
            }
            if case .success(_) = state {
                XCTFail("state shouldnt get to success")
                completionExpectation.fulfill()
            }
        }.store(in: &subcription)
        
        wait(for: [completionExpectation], timeout: 1)
    }
    
    func test_whenSelectedSchoolNameIsNotNilButScores_fetchSchoolDetailsPublishesSuccessWithCustomScoresState() {
        //Given
        let completionExpectation = XCTestExpectation(description: "call should emit success")
        let expectedResult: String = "-"
        let expectedSchoolName: String = "SchoolName"
        selectedSchoolRepository.schoolId = "d"
        schoolDetailsRepository.shouldFail = false
        schoolDetailsRepository.schoolDetails = SchoolDetails(
            schoolId: "Some",
            schoolName: expectedSchoolName,
            satTestTakers: nil,
            satReadingScore: nil,
            satMathScore: nil,
            satWritingScore: nil
        )
        
        //When
        sut.fetchSchoolDetails()
        
        sut.$state.sink { state in
            //Then
            if case .error = state {
                XCTFail("state shouldnt get to fail")
                completionExpectation.fulfill()
            }
            if case .success(let schoolDetails) = state {
                XCTAssertEqual(schoolDetails.schoolName, expectedSchoolName)
                XCTAssertEqual(schoolDetails.satWritingScore, expectedResult)
                XCTAssertEqual(schoolDetails.satMathScore, expectedResult)
                XCTAssertEqual(schoolDetails.satTestTakers, expectedResult)
                XCTAssertEqual(schoolDetails.satReadingScore, expectedResult)
                completionExpectation.fulfill()
            }
        }.store(in: &subcription)
        wait(for: [completionExpectation], timeout: 1)
    }
    
    func test_whenSelectedSchoolNameIsNotNilButScoresAreS_fetchSchoolDetailsPublishesSuccessWithCustomScoresState() {
        //Given
        let completionExpectation = XCTestExpectation(description: "call should emit success")
        let expectedResult: String = "-"
        selectedSchoolRepository.schoolId = "d"
        schoolDetailsRepository.shouldFail = false
        schoolDetailsRepository.schoolDetails = SchoolDetails(
            schoolId: "01",
            schoolName: "SchoolName",
            satTestTakers: "s",
            satReadingScore: "s",
            satMathScore: "s",
            satWritingScore: "s"
        )
        
        //When
        sut.fetchSchoolDetails()
        
        sut.$state.sink { state in
            //Then
            if case .error = state {
                XCTFail("state shouldnt get to fail")
                completionExpectation.fulfill()
            }
            if case .success(let schoolDetails) = state {
                XCTAssertEqual(schoolDetails.satWritingScore, expectedResult)
                XCTAssertEqual(schoolDetails.satMathScore, expectedResult)
                XCTAssertEqual(schoolDetails.satTestTakers, expectedResult)
                XCTAssertEqual(schoolDetails.satReadingScore, expectedResult)
                completionExpectation.fulfill()
            }
        }.store(in: &subcription)
        wait(for: [completionExpectation], timeout: 1)
    }
}
