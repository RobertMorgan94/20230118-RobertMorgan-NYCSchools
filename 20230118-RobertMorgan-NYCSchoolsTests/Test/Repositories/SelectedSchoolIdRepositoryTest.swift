//
//  SelectedSchoolIdRepository.swift
//  20230118-RobertMorgan-NYCSchoolsTests
//
//  Created by Roberto Moran on 1/18/23.
//

@testable import _0230118_RobertMorgan_NYCSchools
import XCTest
import Foundation

final class SelectedSchoolIdRepositoryTest: XCTestCase {
    
    var sut: SelectedSchoolIdRepository!
    var localDataSource: SchoolIdLocalDataSourceMock!
    
    override func setUpWithError() throws {
        try super.setUpWithError()
        localDataSource = SchoolIdLocalDataSourceMock()
        DependencyInitializer.container.register(LocalSchoolIdDataSourceContract.self) { self.localDataSource }
        sut = SelectedSchoolIdRepository()
    }
    
    override func tearDownWithError() throws {
        sut = nil
        localDataSource = nil
        try super.tearDownWithError()
    }
    
    func test_whenDataSourceHasNoId_fetchReturnsNil() {
        // Given
        localDataSource.schoolId = nil
        
        // when
        let result = sut.fetchSelectedSchoolId()
        
        // Then
        XCTAssertNil(result)
    }
    
    func test_whenDataSourceId_fetchReturnsSuchId() {
        // Given
        let id = "anId"
        localDataSource.schoolId = id
        
        // when
        let result = sut.fetchSelectedSchoolId()
        
        // Then
        XCTAssertEqual(result, id)
    }
    
    func test_persistCallsPassesIdToDataSource() {
        // Given
        let id = "anId"
        
        // When
        sut.persist(id)
        
        //Then
        XCTAssertEqual(id, localDataSource.schoolId)
    }
}
