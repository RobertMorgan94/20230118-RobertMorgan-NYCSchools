//
//  SchoolsRepositoryTest.swift
//  20230118-RobertMorgan-NYCSchoolsTests
//
//  Created by Roberto Moran on 1/18/23.
//

@testable import _0230118_RobertMorgan_NYCSchools
import XCTest
import Combine

final class SchoolsRepositoryTest: XCTestCase {
    var sut: SchoolsRepository!
    var remoteDataSource: SchoolsRemoteDataSourceMock!
    
    override func setUpWithError() throws {
        try super.setUpWithError()
        remoteDataSource = SchoolsRemoteDataSourceMock()
        DependencyInitializer.container.register(SchoolsRemoteDataSourceContract.self) { self.remoteDataSource }
        sut = SchoolsRepository()
    }
    
    override func tearDownWithError() throws {
        remoteDataSource = nil
        sut = nil
        try super.tearDownWithError()
    }
    
    func test_whenRemoteSourceFails_fetchSchoolsPublisherAlsoFails() {
        // Given
        let completionExpectation = XCTestExpectation(description: "call should emit failure")
        var subscriptions = [AnyCancellable]()
        remoteDataSource.shouldFail = true
        
        // when
        sut.fetchSchools().sink { completion in
            
            // Then
            switch completion {
            case .finished:
                XCTFail("test should finish without error")
                completionExpectation.fulfill()
            case .failure(_):
                completionExpectation.fulfill()
            }
        } receiveValue: { schools in
            XCTFail("test shouldnt emmit values")
            completionExpectation.fulfill()
        }.store(in: &subscriptions)
        wait(for: [completionExpectation], timeout: 1)
    }
    
    func test_whenRemoteEmitsValues_fetchSchoolsPublishershouldAlsoEmitSaidValues() {
        // Given
        let completionExpectation = XCTestExpectation(description: "call should finish successfully")
        let resultsExpectation = XCTestExpectation(description: "Values should get emmited")
        var subscriptions = [AnyCancellable]()
        let schoolId = "schoolId"
        remoteDataSource.schools = [School(schoolId: schoolId, schoolName: "", neighborhood: "", website: "")]
        remoteDataSource.shouldFail = false
        
        // when
        sut.fetchSchools().sink { completion in
            
            // Then
            switch completion {
            case .finished:
                completionExpectation.fulfill()
            case .failure(_):
                XCTFail("test shouldnt finish with a failure")
                completionExpectation.fulfill()
            }
        } receiveValue: { schools in
            XCTAssertFalse(schools.isEmpty)
            XCTAssert(schools.first!.schoolId == schoolId)
            resultsExpectation.fulfill()
        }.store(in: &subscriptions)
        wait(for: [completionExpectation, resultsExpectation], timeout: 1)
    }
}
