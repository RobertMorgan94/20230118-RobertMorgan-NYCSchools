//
//  SchoolDetailsRepositoryTest.swift
//  20230118-RobertMorgan-NYCSchoolsTests
//
//  Created by Roberto Moran on 1/18/23.
//

@testable import _0230118_RobertMorgan_NYCSchools
import XCTest
import Combine
final class SchoolDetailsRepositoryTest: XCTestCase {
    
    var sut: SchoolDetailsRepository!
    var remoteDataSource: SchoolDetailsRemoteDataSourceMock!
    
    override func setUpWithError() throws {
        try super.setUpWithError()
        remoteDataSource = SchoolDetailsRemoteDataSourceMock()
        DependencyInitializer.container.register(SchoolDetailsRemoteDataSourceContract.self) { self.remoteDataSource }
        sut = SchoolDetailsRepository()
    }
    
    override func tearDownWithError() throws {
        remoteDataSource = nil
        sut = nil
        try super.tearDownWithError()
    }
    
    func test_whenRemoteSourceFails_fetchSchoolsPublisherAlsoFails() {
        // Given
        let schoolId:String = "someId"
        let completionExpectation = XCTestExpectation(description: "call should emit failure")
        var subscriptions = [AnyCancellable]()
        remoteDataSource.shouldFail = true
        
        // when
        sut.fetchDetailsFor(schoolWith: schoolId).sink { completion in
            // Then
            switch completion {
            case .finished:
                XCTFail("test should finish without error")
                completionExpectation.fulfill()
            case .failure(_):
                completionExpectation.fulfill()
            }
        } receiveValue: { schools in
            XCTFail("test shouldnt emmit values")
            completionExpectation.fulfill()
        }.store(in: &subscriptions)
        wait(for: [completionExpectation], timeout: 1)
    }
    
    func test_whenRemoteEmitsAMatchingSchool_fetchSchoolsPublishershouldAlsoEmitSaidSchool() {
        // Given
        let schoolId: String = "01"
        let readingScore: String = "490"
        let completionExpectation = XCTestExpectation(description: "call should finish successfully")
        let resultsExpectation = XCTestExpectation(description: "Values should get emmited")
        var subscriptions = [AnyCancellable]()
        remoteDataSource.schoolDetails = SchoolDetails(
            schoolId: schoolId,
            schoolName: "SchoolName",
            satTestTakers: "Takers",
            satReadingScore: readingScore,
            satMathScore: "Math",
            satWritingScore: "Writing"
        )
        remoteDataSource.shouldFail = false
        
        // when
        sut.fetchDetailsFor(schoolWith: schoolId).sink { completion in
            // Then
            switch completion {
            case .finished:
                completionExpectation.fulfill()
            case .failure(_):
                XCTFail("test shouldnt finish with a failure")
                completionExpectation.fulfill()
            }
        } receiveValue: { schoolDetails in
            XCTAssert( schoolDetails.schoolId == schoolId)
            XCTAssert( schoolDetails.satReadingScore == readingScore)
            resultsExpectation.fulfill()
        }.store(in: &subscriptions)
        wait(for: [completionExpectation, resultsExpectation], timeout: 1)
    }
}
