//
//  LocalSchoolIdSourceTest.swift
//  20230118-RobertMorgan-NYCSchoolsTests
//
//  Created by Roberto Moran on 1/18/23.
//

@testable import _0230118_RobertMorgan_NYCSchools
import XCTest

final class LocalSchoolIdSourceTest: XCTestCase {
    
    var sut: LocalSchoolIdDataSourceContract!
    
    override func setUpWithError() throws {
        try super.setUpWithError()
        sut = InMemorySchoolIdDataSource()
    }
    
    override func tearDownWithError() throws {
        sut = nil
        try super.tearDownWithError()
    }
    
    func test_ifNoSchoolIdHasBeenPersist_fetchReturnsNil() {
        // When
        let result = sut.fetchSelectedSchoolId()
        
        // Then
        XCTAssertNil(result)
    }
    
    func test_ifSchoolIdHasBeenPersist_fetchReturnThePersistedId() {
        // Given
        let id: String = "01"
        
        // When
        sut.persist(id: id)
        let result = sut.fetchSelectedSchoolId()
        
        // Then
        XCTAssertEqual(result, id)
    }
    
    func test_afterPersistingTwoIds_fetchReturnsTheSecondId() {
        // Given
        sut.persist(id: "00")
        let id: String = "01"
        
        // When
        sut.persist(id: id)
        let result = sut.fetchSelectedSchoolId()
        
        // Then
        XCTAssertEqual(result, id)
    }
}
