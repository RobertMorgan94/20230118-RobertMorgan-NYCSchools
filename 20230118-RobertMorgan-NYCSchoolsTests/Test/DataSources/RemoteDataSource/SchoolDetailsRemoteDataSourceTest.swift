//
//  SchoolDetailsRemoteDataSourceTest.swift
//  20230118-RobertMorgan-NYCSchoolsTests
//
//  Created by Roberto Moran on 1/18/23.
//

@testable import _0230118_RobertMorgan_NYCSchools
import XCTest
import Combine

final class SchoolDetailsRemoteDataSourceTest: XCTestCase {
    
    var sut: SchoolDetailsRemoteDataSourceContract!
    
    override func setUpWithError() throws {
        try super.setUpWithError()
        sut = SchoolDetailsRemoteDataSource()
    }
    
    override func tearDownWithError() throws {
        sut = nil
        try super.tearDownWithError()
    }
    
    func test_apiCallIsRight() {
        //Given
        let schoolDetails: String = "11X253"
        let completionExpectation = XCTestExpectation(description: "call should complete successfully")
        let resultExpectation = XCTestExpectation(description: "call should emit at least one value before completing")
        var subscriptions = [AnyCancellable]()
        
        //When
        sut.fetchDetailsFor(schoolWith: schoolDetails).sink { completion in
            
            //Then
            switch completion {
            case .finished: completionExpectation.fulfill()
            case .failure(_):
                XCTFail("Call shouldnt fail unless there is no internet access")
                resultExpectation.fulfill()
                completionExpectation.fulfill()
            }
        } receiveValue: { _ in
            resultExpectation.fulfill()
        }.store(in: &subscriptions)
        wait(for: [completionExpectation, resultExpectation], timeout: 10)
    }
    
    func test_ifUrlIsMalformed_errorIsTriggered() {
        // Given
        let schoolDetails: String = "someSchoolDetail"
        let completionExpectation = XCTestExpectation(description: "call should complete successfully")
        let resultExpectation = XCTestExpectation(description: "call should emit at least one value before completing")
        var subscriptions = [AnyCancellable]()
        
        //When
        sut.fetchDetailsFor(schoolWith: schoolDetails).sink { completion in
            
            // Then
            switch completion {
            case .finished: completionExpectation.fulfill()
                XCTFail("Call shouldnt finish gracefully unless there is no internet access")
                completionExpectation.fulfill()
            case .failure(_):
                completionExpectation.fulfill()
            }
        } receiveValue: { schools in
            XCTFail("Call shouldnt yield any values")
        }.store(in: &subscriptions)
        wait(for: [completionExpectation], timeout: 1)
    }
}
