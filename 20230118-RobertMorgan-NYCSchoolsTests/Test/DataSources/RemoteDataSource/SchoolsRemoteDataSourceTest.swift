//
//  SchoolsRemoteDataSourceTest.swift
//  20230118-RobertMorgan-NYCSchoolsTests
//
//  Created by Roberto Moran on 1/18/23.
//

@testable import _0230118_RobertMorgan_NYCSchools
import XCTest
import Combine

final class SchoolsRemoteDataSourceTest: XCTestCase {
    
    var sut: SchoolsRemoteDataSourceContract!
    
    override func setUpWithError() throws {
        try super.setUpWithError()
        sut = SchoolsRemoteDataSource()
    }
    
    override func tearDownWithError() throws {
        sut = nil
        try super.tearDownWithError()
    }
    
    // TODO: Add a check to ignore the test if there is no network.
    func test_apiCallIsRight() {
        // Given
        let completionExpectation = XCTestExpectation(description: "call should complete successfully")
        let resultExpectation = XCTestExpectation(description: "call should emit at least one set of schools")
        var subscriptions = [AnyCancellable]()
        
        // when
        sut.fetchSchools().sink { completion in
            
            // Then
            switch completion {
            case .finished: completionExpectation.fulfill()
            case .failure(_):
                XCTFail("Call shouldnt fail unless there is no internet access")
                completionExpectation.fulfill()
            }
        } receiveValue: { schools in
            XCTAssert(!schools.isEmpty)
            resultExpectation.fulfill()
        }.store(in: &subscriptions)
        wait(for: [completionExpectation, resultExpectation], timeout: 10)
    }
    
    func test_ifUrlIsMalformed_errorIsTriggered() {
        // Given
        DependencyInitializer.container.register(SchoolsApiConfigContract.self) { SchoolsApiConfigBadURLMock() }
        sut = SchoolsRemoteDataSource()
        let completionExpectation = XCTestExpectation(description: "call should complete with a failure")
        var subscriptions = [AnyCancellable]()
        
        // when
        sut.fetchSchools().sink { completion in
            
            // Then
            switch completion {
            case .finished: completionExpectation.fulfill()
                XCTFail("Call shouldnt finish gracefully unless there is no internet access")
                completionExpectation.fulfill()
            case .failure(_):
                completionExpectation.fulfill()
            }
        } receiveValue: { schools in
            XCTFail("Call shouldnt yield any values")
        }.store(in: &subscriptions)
        wait(for: [completionExpectation], timeout: 1)
    }
}
