//
//  MainView.swift
//  20230118-RobertMorgan-NYCSchools
//
//  Created by Roberto Moran on 1/19/23.
//

import SwiftUI

struct MainView: View {
    
    @ObservedObject
    private var navigationManager: NavigationManager
    
    init() {
        @Inject var navigationManager: NavigationManager
        self.navigationManager = navigationManager
    }
    
    var body: some View {
        NavigationStack(path: $navigationManager.path) {
            SchoolsView()
                .navigationDestination(for: Destinations.self) { destination in
                    switch destination {
                    case .details: SchoolDetailsView()
                    }
                }
        }
    }
}
