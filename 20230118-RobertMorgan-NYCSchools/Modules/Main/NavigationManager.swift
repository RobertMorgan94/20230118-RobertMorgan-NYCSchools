//
//  NavigationManager.swift
//  20230118-RobertMorgan-NYCSchools
//
//  Created by Roberto Moran on 1/19/23.
//

import Foundation

class NavigationManager: NavigationDelegate, ObservableObject {
    @Published var path: [Destinations] = []
    
    func push(destination: Destinations) {
        DispatchQueue.main.asyncAfter(deadline: .now()) { [weak self] in
            self?.path.append(destination)
        }
    }
}
