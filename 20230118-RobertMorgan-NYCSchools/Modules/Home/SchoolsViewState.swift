//
//  SchoolsViewState.swift
//  20230118-RobertMorgan-NYCSchools
//
//  Created by Roberto Moran on 1/19/23.
//

import Foundation

enum SchoolsViewState {
    case none
    case loading
    case error
    case success(schools: [DisplayableSchool])
}
