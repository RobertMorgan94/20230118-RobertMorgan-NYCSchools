//
//  SchoolsRepositoryContract.swift
//  20230118-RobertMorgan-NYCSchools
//
//  Created by Roberto Moran on 1/19/23.
//

import Foundation
import Combine

protocol SchoolsRepositoryContract {
    func fetchSchools() -> AnyPublisher<[School], Error>
}
