//
//  HomeViewModel.swift
//  20230118-RobertMorgan-NYCSchools
//
//  Created by Roberto Moran on 1/18/23.
//

import Foundation
import Combine

class HomeViewModel: ObservableObject {
    
    private var subscriptions: Set<AnyCancellable> = []
    
    @Inject
    private var repository: any SchoolsRepositoryContract
    
    @Inject
    private var selectedSchoolRepository: any SelectedSchoolIdRepositoryContract
    
    @Inject
    private var navigationDelegate: any NavigationDelegate
    
    @Published var state: SchoolsViewState = .none
    
    func fetchSchools() {
        if case .loading = state {
            return
        }
        if case .success(_) = state {
            return
        }
        state = .loading
        repository.fetchSchools()
            .receive(on: DispatchQueue.main)
            .sink { [weak self] completion in
                switch completion {
                case .finished: break
                case .failure(_): self?.state = .error
                }
            } receiveValue: { [weak self] schools in
                if schools.isEmpty {
                    self?.state = .error
                } else {
                    guard let displayableSchools = self?.getDisplayableSchools(from: schools), !displayableSchools.isEmpty else {
                        self?.state = .error
                        return
                    }
                    self?.state = .success(schools: displayableSchools)
                }
            }
            .store(in: &subscriptions)
    }
    
    private func getDisplayableSchools(from schools: [School]) -> [DisplayableSchool] {
        return schools.compactMap { school in
            guard let name = school.schoolName, let neighborhood = school.neighborhood, let id = school.schoolId else {
                return nil
            }
            return DisplayableSchool(name: name, id: id, neighborhood: neighborhood)
        }
    }
    
    func onSchoolSelected(withId id: String) {
        selectedSchoolRepository.persist(id)
        navigationDelegate.push(destination: .details)
    }
}
