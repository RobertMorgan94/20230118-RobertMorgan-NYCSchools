//
//  SchoolView.swift
//  20230118-RobertMorgan-NYCSchools
//
//  Created by Roberto Moran on 1/18/23.
//

import SwiftUI

struct SchoolRowItemView: View {
    
    let school: DisplayableSchool
    let onSchoolClicked: (_ withId: String) -> Void
    
    init(school: DisplayableSchool, onSchoolClicked: @escaping (_ withId: String) -> Void) {
        self.school = school
        self.onSchoolClicked = onSchoolClicked
    }
    
    var body: some View {
        Button {
            onSchoolClicked(school.id)
        } label: {
            VStack(alignment: .leading) {
                Text(school.name)
                    .font(.callout)
                    .foregroundColor(.black)
                Text("Neighborhood: \(school.neighborhood)")
                    .font(.footnote)
                    .foregroundColor(Color(.systemGray))
            }
            .padding(.top)
        }
    }
}

struct SchoolView_Previews: PreviewProvider {
    static var previews: some View {
        SchoolRowItemView(school: DisplayableSchool(name: "Oxford", id: "", neighborhood: "a great neighborhood"), onSchoolClicked: { _ in })
    }
}
