//
//  ContentView.swift
//  20230118-RobertMorgan-NYCSchools
//
//  Created by Roberto Moran on 1/18/23.
//

import SwiftUI

struct SchoolsView: View {
    
    @ObservedObject
    var viewmodel: HomeViewModel
    
    init() {
        @Inject var viewmodel: HomeViewModel
        self.viewmodel = viewmodel
        viewmodel.fetchSchools()
    }
    
    var body: some View {
        NavigationView {
            getViewFor(state: viewmodel.state)
                .navigationTitle("NYC schools")
        }
    }
    
    @ViewBuilder
    private func getViewFor(state: SchoolsViewState) -> some View {
        switch state {
        case .none: EmptyView()
        case .loading: ProgressView()
        case .error: produceErrorView()
        case .success(let schools): produceSuccessView(with: schools)
        }
    }
    
    @ViewBuilder
    private func produceErrorView() -> some View {
        ErrorMessageView(errorMessage: "Oops something went wrong", retryMessage: "Click to retry") { viewmodel.fetchSchools() }
    }
    
    @ViewBuilder
    private func produceSuccessView(with schools: [DisplayableSchool]) -> some View {
        GeometryReader { geometry in
            List(schools) { school in
                SchoolRowItemView(school: school) { schoolId in
                    self.viewmodel.onSchoolSelected(withId: schoolId)
                }
            }
            .padding(.bottom, geometry.safeAreaInsets.bottom)
        }
    }
}

struct ContentView_Previews: PreviewProvider {
    static var previews: some View {
        NavigationStack {
            SchoolsView()
        }
    }
}
