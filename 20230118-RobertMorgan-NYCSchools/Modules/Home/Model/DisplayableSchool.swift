//
//  DisplayableSchool.swift
//  20230118-RobertMorgan-NYCSchools
//
//  Created by Roberto Moran on 1/18/23.
//

import Foundation

struct DisplayableSchool: Identifiable {
    let name: String
    let id: String
    let neighborhood: String
}
