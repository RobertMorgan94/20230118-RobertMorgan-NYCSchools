//
//  SchoolDetailsViewModel.swift
//  20230118-RobertMorgan-NYCSchools
//
//  Created by Roberto Moran on 1/19/23.
//

import Foundation
import Combine

class SchoolDetailsViewModel: ObservableObject {
    
    private let noInformationFoundDefaultMessage = "-"
    private var subscriptions: Set<AnyCancellable> = []
    
    @Inject
    private var repository: any SchoolDetailsRepositoryContract
    
    @Inject
    private var selectedSchoolRepository: any SelectedSchoolIdRepositoryContract
    
    @Published var state: SchoolDetailsViewState = .none
    
    func fetchSchoolDetails() {
        if case .loading = state {
            return
        }
        state = .loading
        guard let schoolId = selectedSchoolRepository.fetchSelectedSchoolId() else {
            state = .error
            return
        }
        repository.fetchDetailsFor(schoolWith: schoolId)
            .receive(on: DispatchQueue.main)
            .sink { [weak self] completion in
                switch completion {
                case .finished: break
                case .failure(_): self?.state = .error
                }
            } receiveValue: { [weak self] school in
                guard let schoolDetails = self?.getDisplayableSchoolDetails(from: school) else {
                    self?.state = .error
                    return
                }
                self?.state = .success(schoolDetails: schoolDetails)
            }
            .store(in: &subscriptions)
    }
    
    private func getDisplayableSchoolDetails(from school: SchoolDetails) -> DisplayableSchoolDetails? {
        guard let name = school.schoolName else {
            return nil
        }
        let satTestTakers = getValidStringFor(word: school.satTestTakers)
        let satReadingScore = getValidStringFor(word: school.satReadingScore)
        let satMathScore = getValidStringFor(word: school.satMathScore)
        let satWritingScore = getValidStringFor(word: school.satWritingScore)
        return DisplayableSchoolDetails(schoolName: name,
                                        satTestTakers: satTestTakers,
                                        satReadingScore: satReadingScore,
                                        satMathScore: satMathScore,
                                        satWritingScore: satWritingScore)
    }
    
    private func getValidStringFor(word: String?) -> String {
        guard let word = word, word != "s" else {
            return noInformationFoundDefaultMessage
        }
        return word
    }
}
