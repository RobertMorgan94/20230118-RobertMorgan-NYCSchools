//
//  DisplayableSchoolDetails.swift
//  20230118-RobertMorgan-NYCSchools
//
//  Created by Roberto Moran on 1/19/23.
//

import Foundation

struct DisplayableSchoolDetails {
    let schoolName : String
    let satTestTakers: String
    let satReadingScore: String
    let satMathScore: String
    let satWritingScore: String
}
