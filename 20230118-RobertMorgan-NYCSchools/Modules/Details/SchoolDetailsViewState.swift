//
//  SchoolDetailsViewState.swift
//  20230118-RobertMorgan-NYCSchools
//
//  Created by Roberto Moran on 1/19/23.
//

import Foundation

enum SchoolDetailsViewState {
    case none
    case loading
    case error
    case success(schoolDetails: DisplayableSchoolDetails)
}
