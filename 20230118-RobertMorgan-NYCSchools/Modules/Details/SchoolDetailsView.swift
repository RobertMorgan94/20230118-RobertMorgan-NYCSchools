//
//  SchoolDetailsView.swift
//  20230118-RobertMorgan-NYCSchools
//
//  Created by Roberto Moran on 1/19/23.
//

import SwiftUI

struct SchoolDetailsView: View {
    
    @ObservedObject
    var viewmodel: SchoolDetailsViewModel
    
    init() {
        @Inject var viewmodel: SchoolDetailsViewModel
        self.viewmodel = viewmodel
        viewmodel.fetchSchoolDetails()
    }
    
    var body: some View {
        NavigationView {
            switch viewmodel.state {
            case .none: EmptyView()
            case .loading: ProgressView()
            case .error: produceErrorView()
            case .success(let school): produceSuccessView(with: school)
            }
        }
    }
    
    @ViewBuilder
    private func produceErrorView() -> some View {
        ErrorMessageView(errorMessage: "Oops something went wrong", retryMessage: "Click to retry") { viewmodel.fetchSchoolDetails() }
    }
    
    @ViewBuilder
    private func produceSuccessView(with school: DisplayableSchoolDetails) -> some View {
        VStack(alignment: .leading) {
            Text(school.schoolName)
                .font(.title3)
                .padding(.vertical)
            Text("SAT information:")
                .font(.headline)
                .padding(.vertical)
            Text("Number of participants: \(school.satTestTakers)")
                .font(.callout)
                .padding(.bottom)
            Text("Math score: \(school.satMathScore)")
                .font(.callout)
                .padding(.bottom)
            Text("Writing score: \(school.satWritingScore)")
                .font(.callout)
                .padding(.bottom)
            Text("Reading score: \(school.satReadingScore)")
                .font(.callout)
                .padding(.bottom)
            Spacer()
        }
        .padding(.horizontal)
    }
}

struct SchoolDetailsView_Previews: PreviewProvider {
    static var previews: some View {
        SchoolDetailsView()
    }
}
