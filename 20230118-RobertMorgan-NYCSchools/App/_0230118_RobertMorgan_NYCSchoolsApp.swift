//
//  _0230118_RobertMorgan_NYCSchoolsApp.swift
//  20230118-RobertMorgan-NYCSchools
//
//  Created by Roberto Moran on 1/18/23.
//

import SwiftUI

@main
struct _0230118_RobertMorgan_NYCSchoolsApp: App {
    
    init() {
        // Initializes dependencies
        _ = DependencyInitializer()
    }
    
    var body: some Scene {
        WindowGroup {
            MainView()
        }
    }
}
