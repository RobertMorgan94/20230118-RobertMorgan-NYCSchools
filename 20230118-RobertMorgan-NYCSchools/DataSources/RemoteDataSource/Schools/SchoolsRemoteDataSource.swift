//
//  SchoolsRemoteDataSource.swift
//  20230118-RobertMorgan-NYCSchools
//
//  Created by Roberto Moran on 1/18/23.
//

import Foundation
import Combine

class SchoolsRemoteDataSource: SchoolsRemoteDataSourceContract {
    
    @Inject
    private var apiConfig: SchoolsApiConfigContract
    
    @Inject
    private var decoder: JSONDecoder
    
    @Inject
    private var session: URLSession
    
    func fetchSchools() -> AnyPublisher<[School], Error> {
        guard let url = apiConfig.getSchoolsUrl() else {
            return Fail(error: RemoteDataSourceErrors.invalidURL).eraseToAnyPublisher()
        }
        
        return session.dataTaskPublisher(for: url)
            .map(\.data)
            .decode(type: [School].self, decoder: decoder)
            .eraseToAnyPublisher()
    }
}
