//
//  SchoolsApiConfigContract.swift
//  20230118-RobertMorgan-NYCSchools
//
//  Created by Roberto Moran on 1/19/23.
//

import Foundation

protocol SchoolsApiConfigContract {
    func getSchoolsUrl() -> URL?
}
