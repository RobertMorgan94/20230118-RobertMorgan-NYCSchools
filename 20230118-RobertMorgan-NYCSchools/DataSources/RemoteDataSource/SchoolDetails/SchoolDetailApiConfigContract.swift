//
//  SchoolDetailApiConfigContract.swift
//  20230118-RobertMorgan-NYCSchools
//
//  Created by Roberto Moran on 1/19/23.
//

import Foundation

protocol SchoolDetailApiConfigContract {
    func generateUrl(forSchoolWithId id: String) -> URL?
}
