//
//  SchoolDetailsRemoteDataSource.swift
//  20230118-RobertMorgan-NYCSchools
//
//  Created by Roberto Moran on 1/18/23.
//

import Foundation
import Combine

class SchoolDetailsRemoteDataSource: SchoolDetailsRemoteDataSourceContract {
    
    @Inject
    private var apiConfig: SchoolDetailApiConfigContract
    
    @Inject
    private var decoder: JSONDecoder
    
    @Inject
    private var session: URLSession
    
    func fetchDetailsFor(schoolWith id: String) -> AnyPublisher<SchoolDetails, Error> {
        guard let url = apiConfig.generateUrl(forSchoolWithId: id) else {
            return Fail(error: RemoteDataSourceErrors.invalidURL).eraseToAnyPublisher()
        }
        
        return session.dataTaskPublisher(for: url)
            .map(\.data)
            .decode(type: [SchoolDetails].self, decoder: decoder)
            .tryMap({ matchingSchools in
                guard let first = matchingSchools.first else {
                    throw RemoteDataSourceErrors.noDataFound
                }
                return first
            })
            .eraseToAnyPublisher()
    }
}
