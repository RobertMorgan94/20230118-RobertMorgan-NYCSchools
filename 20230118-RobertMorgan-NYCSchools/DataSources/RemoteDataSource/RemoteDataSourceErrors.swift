//
//  NetworkResponses.swift
//  20230118-RobertMorgan-NYCSchools
//
//  Created by Roberto Moran on 1/18/23.
//

import Foundation

enum RemoteDataSourceErrors: Error {
    case invalidURL, noDataFound
}
