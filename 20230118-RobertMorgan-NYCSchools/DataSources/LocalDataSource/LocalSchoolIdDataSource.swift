//
//  LocalSchoolIdDataSource.swift
//  20230118-RobertMorgan-NYCSchools
//
//  Created by Roberto Moran on 1/18/23.
//

import Foundation

class InMemorySchoolIdDataSource: LocalSchoolIdDataSourceContract {
    
    private var schoolId: String?
    
    func persist(id: String) {
        self.schoolId = id
    }
    
    func fetchSelectedSchoolId() -> String? {
        return schoolId
    }
}
