//
//  SchoolDetailsRepository.swift
//  20230118-RobertMorgan-NYCSchools
//
//  Created by Roberto Moran on 1/18/23.
//

import Foundation
import Combine

class SchoolDetailsRepository: SchoolDetailsRepositoryContract {
    
    @Inject
    private var remoteDataSource: SchoolDetailsRemoteDataSourceContract
    
    func fetchDetailsFor(schoolWith id: String) -> AnyPublisher<SchoolDetails, Error> {
        return remoteDataSource.fetchDetailsFor(schoolWith: id)
    }
}
