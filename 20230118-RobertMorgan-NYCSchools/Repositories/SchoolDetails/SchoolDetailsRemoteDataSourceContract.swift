//
//  SchoolDetailsRemoteDataSourceContract.swift
//  20230118-RobertMorgan-NYCSchools
//
//  Created by Roberto Moran on 1/19/23.
//

import Foundation
import Combine

protocol SchoolDetailsRemoteDataSourceContract {
    func fetchDetailsFor(schoolWith id: String) -> AnyPublisher<SchoolDetails, Error>
}
