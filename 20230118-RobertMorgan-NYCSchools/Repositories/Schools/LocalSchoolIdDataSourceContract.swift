//
//  LocalSchoolIdDataSourceContract.swift
//  20230118-RobertMorgan-NYCSchools
//
//  Created by Roberto Moran on 1/19/23.
//

import Foundation

protocol LocalSchoolIdDataSourceContract {
    func persist(id: String)
    func fetchSelectedSchoolId() -> String?
}
