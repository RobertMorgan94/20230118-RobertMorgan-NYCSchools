//
//  SelectedSchoolRepository.swift
//  20230118-RobertMorgan-NYCSchools
//
//  Created by Roberto Moran on 1/18/23.
//

import Foundation

class SelectedSchoolIdRepository: SelectedSchoolIdRepositoryContract {
    
    @Inject
    private var localDataSource: LocalSchoolIdDataSourceContract
    
    func persist(_ id: String) {
        localDataSource.persist(id: id)
    }
    
    func fetchSelectedSchoolId() -> String? {
        return localDataSource.fetchSelectedSchoolId()
    }
}
