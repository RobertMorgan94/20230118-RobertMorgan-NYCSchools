//
//  SchoolsRepository.swift
//  20230118-RobertMorgan-NYCSchools
//
//  Created by Roberto Moran on 1/18/23.
//

import Foundation
import Combine

class SchoolsRepository: SchoolsRepositoryContract {
    
    @Inject
    private var remoteDataSource: SchoolsRemoteDataSourceContract
    
    func fetchSchools() -> AnyPublisher<[School], Error> {
        return remoteDataSource.fetchSchools()
    }
}
