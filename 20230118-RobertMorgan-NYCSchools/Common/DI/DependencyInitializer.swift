//
//  DependencyInitializer.swift
//  20230118-RobertMorgan-NYCSchools
//
//  Created by Roberto Moran on 1/18/23.
//

import Foundation

class DependencyInitializer {
    static let container = Container()
    
    init() {
        addDependencies(to: DependencyInitializer.container)
    }
    
    func addDependencies(to container: Container) {
        
        container.register(SchoolsApiConfigContract.self) { SchoolsApiConfig() }
        container.register(SchoolDetailApiConfigContract.self) { SchoolDetailApiConfig() }
        
        let navigationManager = NavigationManager()
        container.register(NavigationManager.self) { navigationManager }
        container.register(NavigationDelegate.self) { navigationManager }
        
        let sharedDecoder = JSONDecoder()
        container.register(JSONDecoder.self) { sharedDecoder }
        container.register(URLSession.self) { URLSession.shared }
        
        container.register(SchoolsRemoteDataSourceContract.self) { SchoolsRemoteDataSource() }
        container.register(SchoolDetailsRemoteDataSourceContract.self) { SchoolDetailsRemoteDataSource() }
        
        let schoolIdLocalDataSource = InMemorySchoolIdDataSource()
        container.register(LocalSchoolIdDataSourceContract.self) { schoolIdLocalDataSource }
        
        container.register(SelectedSchoolIdRepositoryContract.self) { SelectedSchoolIdRepository() }
        container.register(SchoolsRepositoryContract.self) { SchoolsRepository() }
        container.register(SchoolDetailsRepositoryContract.self) { SchoolDetailsRepository()}
        
        let homeViewModel = HomeViewModel()
        container.register(HomeViewModel.self) { homeViewModel }
        container.register(SchoolDetailsViewModel.self) { SchoolDetailsViewModel() }
    }
}
