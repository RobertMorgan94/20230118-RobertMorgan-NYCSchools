//
//  Inject.swift
//  20230118-RobertMorgan-NYCSchools
//
//  Created by Roberto Moran on 1/18/23.
//

import Foundation

/*
 Marking a var with this property wrapper allows you to inject with the factory declared at the
 DependencyInitializer's addDependencies method. If you are facing a runtime issue, you might be missing
 the declaration of your factory.
 */
@propertyWrapper
class Inject<T> {
    let wrappedValue: T
    init() {
        if let value = DependencyInitializer.container.resolve(T.self) {
            wrappedValue = value
        } else {
            fatalError("No dependency found for \(T.self)")
        }
    }
}
