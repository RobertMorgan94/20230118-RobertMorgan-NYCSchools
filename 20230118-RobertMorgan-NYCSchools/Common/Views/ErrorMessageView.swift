//
//  ErrorMessageView.swift
//  20230118-RobertMorgan-NYCSchools
//
//  Created by Roberto Moran on 1/18/23.
//

import SwiftUI

struct ErrorMessageView: View {
    
    let errorMessage: String
    let retryMessage: String
    let action: () -> Void
    
    init(errorMessage: String, retryMessage: String, action: @escaping () -> Void) {
        self.errorMessage = errorMessage
        self.retryMessage = retryMessage
        self.action = action
    }
    
    var body: some View {
        VStack {
            Text(errorMessage)
                .padding(.bottom)
            Button {
                action()
            } label: {
                Text(retryMessage)
            }
        }

    }
}

struct ErrorMessageView_Previews: PreviewProvider {
    static var previews: some View {
        ErrorMessageView(errorMessage: "oops, something went wrong", retryMessage: "retry") {}
    }
}
