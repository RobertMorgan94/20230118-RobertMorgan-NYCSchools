//
//  School.swift
//  20230118-RobertMorgan-NYCSchools
//
//  Created by Roberto Moran on 1/18/23.
//

import Foundation

struct School : Codable{
    let schoolId: String?
    let schoolName: String?
    let neighborhood: String?
    let website: String?
    
    enum CodingKeys: String, CodingKey {
        case schoolId = "dbn"
        case schoolName = "school_name"
        case neighborhood
        case website
    }
}
