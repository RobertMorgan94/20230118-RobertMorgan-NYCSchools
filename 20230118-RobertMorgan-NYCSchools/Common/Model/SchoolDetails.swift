//
//  SchoolDetails.swift
//  20230118-RobertMorgan-NYCSchools
//
//  Created by Roberto Moran on 1/18/23.
//

import Foundation

struct SchoolDetails: Codable {
    let schoolId: String?
    let schoolName : String?
    let satTestTakers: String?
    let satReadingScore: String?
    let satMathScore: String?
    let satWritingScore: String?
    
    enum CodingKeys: String, CodingKey {
        case schoolId = "dbn"
        case schoolName = "school_name"
        case satTestTakers = "num_of_sat_test_takers"
        case satReadingScore = "sat_critical_reading_avg_score"
        case satMathScore = "sat_math_avg_score"
        case satWritingScore = "sat_writing_avg_score"
    }
}
