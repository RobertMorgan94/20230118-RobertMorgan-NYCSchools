//
//  NycSchoolNetworkConfig.swift
//  20230118-RobertMorgan-NYCSchools
//
//  Created by Roberto Moran on 1/18/23.
//

import Foundation

struct SchoolsApiConfig: SchoolsApiConfigContract {
    func getSchoolsUrl() -> URL? {
        guard let nycSchoolUrl = URL(string: "https://data.cityofnewyork.us/resource/s3k6-pzi2.json") else {
            return nil
        }
        return nycSchoolUrl
    }
}
