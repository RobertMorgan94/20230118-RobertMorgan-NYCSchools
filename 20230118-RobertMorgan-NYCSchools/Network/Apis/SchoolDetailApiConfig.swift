//
//  SchoolDetailApiConfig.swift
//  20230118-RobertMorgan-NYCSchools
//
//  Created by Roberto Moran on 1/18/23.
//

import Foundation

struct SchoolDetailApiConfig: SchoolDetailApiConfigContract {
    
    private let baseUrl = "https://data.cityofnewyork.us/resource/f9bf-2cp4.json"
    private let schoolIdQueryKey = "dbn"
    
    func generateUrl(forSchoolWithId id: String) -> URL? {
        let queryItems = [URLQueryItem(name: schoolIdQueryKey, value: id)]
        var urlComps = URLComponents(string: baseUrl)
        urlComps?.queryItems = queryItems
        return urlComps?.url
    }
}

